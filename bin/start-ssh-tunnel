#!/usr/bin/env bash

main() {
  if ! is-configured; then
    at missing-configuration
    exec "$@"
  fi

  at starting
  run-ssh-tunnel "$@"
}

run-ssh-tunnel() {
  declare sentinel=/tmp/ssh-tunnel-buildpack-wait
  declare -A pids signals

  config-gen

  # Use named pipe to detect exit of any subprocess.
  rm -f "$sentinel"
  mkfifo "$sentinel"

  # Start processes.
  aux-start ssh-tunnel SIGINT ssh ${SSHTUNNEL_EXTRA_PARAMS} -N ssh-tunnel
  app-start SIGTERM "$@"
  pid=$!
  pgid=$(cut -d' ' -f5 /proc/$pid/stat)

  # Don't exit top script until all subprocesses are done.
  trap '' SIGTERM

  # This read will block the process waiting on a msg to be put into the
  # fifo. If any of the processes defined above should exit, a msg will be
  # put into the fifo causing the read operation to un-block. The process
  # putting the msg into the fifo will use it's process name as a msg so that
  # we can print the offending process to stdout.
  declare exit_process
  read exit_process < "$sentinel"
  at "exit process=$exit_process"

  # Remove the FIFO. This allows following writes to simply create a file,
  # rather than blocking because there's nothing reading the other end.
  rm -f "$sentinel"

  # Clean up any running processes.
  # SIGTERM the application's process group (hence the negative PID), just in
  # case something else crashed. If the dyno is shutting down, then SIGTERM
  # has already been sent.
  at "kill-app pid=$pid pgid=$pgid"
  kill -SIGTERM -$pgid

  # Wait for the app to finish.
  at "wait-app pid=$pid pgid=$pgid"
  wait $pid

  # Kill the auxiliary processes.
  # Send each one SIGHUP which will be translated by the trap in aux-start.
  declare name
  for name in "${!pids[@]}"; do
    at "kill-aux name=$name pid=${pids[$name]} signal=${signals[$name]}"
    kill -SIGHUP "${pids[$name]}"
  done
}

config-gen() {
  echo "${HOME}"
  mkdir -p ${HOME}/.ssh
  chmod 700 ${HOME}/.ssh

  ls "${HOME}"

  cp "${HOME}/heroku-ssh-tunnel" ${HOME}/.ssh/ssh-tunnel-key
  chmod 600 ${HOME}/.ssh/ssh-tunnel-key

  cat >${HOME}/.ssh/config <<EOL
Host ssh-tunnel
  HostName ${SSHTUNNEL_REMOTE_HOST}
  Port ${SSHTUNNEL_REMOTE_PORT:-22}
  User ${SSHTUNNEL_REMOTE_USER}
  IdentityFile ${HOME}/.ssh/ssh-tunnel-key
  RemoteForward 9999 localhost:${PORT}
  ServerAliveInterval 10
  ServerAliveCountMax 3
  StrictHostKeyChecking=no
EOL
}

aux-start() {
  declare name=$1 signal=$2
  shift 2

  (
    at "$name-start"

    # Ignore SIGTERM; this is inherited by the child process.
    trap '' SIGTERM

    # Start child in the background.
    "$@" &

    # Translate SIGHUP to the appropriate signal to stop the child (anything
    # except SIGTERM which is ignored). This *will* cancel the wait and may
    # lead to the outer subshell exiting before the aux process
    trap "kill -$signal $!" SIGHUP

    # Wait for child to finish, either by crash or by $signal
    wait

    # Notify FIFO if this finishes first
    echo "$name" > "$sentinel"

    at "$name-end"
  ) &

  pids[$name]=$!
  signals[$name]=$signal
  at "$name-launched pid=$! signal=$signal"
}

app-start() {
  declare name=app signal=$1
  shift

  (
    at "$name-start"

    # Start child in the background. This is before the trap because
    # the app needs to be able to receive when the dyno broadcasts
    # SIGTERM on shutdown.
    "$@" &

    # Translate SIGHUP to the appropriate signal to stop the child
    # (probably SIGTERM in this case). This *will* cancel the wait and may
    # lead to the outer subshell exiting before the app.
    trap "kill -$signal $!" SIGHUP

    # Ignore SIGTERM because the dyno will broadcast it to all children --
    # there is no need to translate it.
    trap "" SIGTERM

    # Wait for the app to finish, either by crash or by $signal
    wait

    # Notify FIFO if this finishes first
    echo "$name" > $sentinel

    at "$name-end"
  ) &

  at "$name-launched pid=$!"
}

at() {
  echo "buildpack=ssh-tunnel at=$*"
}

is-configured() {
  [[ \
    -v SSHTUNNEL_REMOTE_USER && \
    -v SSHTUNNEL_REMOTE_HOST \
  ]] && return 0 || return 1
}

[[ "$0" != "$BASH_SOURCE" ]] || main "$@"
